import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/services/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Installator, Phase } from '@app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class LandingPageService {

  constructor(
    private api: ApiService
  ) { }

  urlValidation(endpoint: string): Observable<any> {
    return this.api.options(`${endpoint}/installation`).pipe(
      map(
        resp => {
          return resp;
        },
        error => {
          throw error;
        }
      )
    );
  }

  getComponents(endpoint: string): Observable<Installator> {
    return this.api.get(`${endpoint}/installation`).pipe(
      map(
        resp => {
          return resp;
        },
        error => {
          throw error;
        }
      )
    );
  }

  getPhases(endpoint: string): Observable<Phase[]> {
    return this.api.get(`${endpoint}/phases`).pipe(
      map(
        resp => {
          return resp.allPhases;
        },
        error => {
          throw error;
        }
      )
    );
  }
}




