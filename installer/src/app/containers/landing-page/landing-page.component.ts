import { Component, OnInit } from '@angular/core';
import { LandingPageService } from './landing-page.service';
import { Installator, Phase } from '@app/shared/models';
import { ApiService } from '@app/core/services/api.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  search: string;
  loading = false;

  showError = false;
  errorMessage = '';

  showSuccess = false;
  successMessage = '';

  installator: Installator;
  phases: Phase[];

  constructor(
    private landingPageService: LandingPageService
  ) { }

  ngOnInit(): void {
    if (window.localStorage.getItem('searchBarInput')) {
      this.search = window.localStorage.getItem('searchBarInput');
      this.urlValidation(this.search);
    }
  }

  urlValidation(endpoint: string): void {
    this.toggleLoader();
    window.localStorage.setItem('searchBarInput', this.search);
    this.landingPageService.urlValidation(this.search).subscribe(
      resp => {
        this.toggleLoader();
        this.getComponents(endpoint);
        this.getPhases(endpoint);
      },
      error => {
        console.log(error);
        this.toggleLoader();
        this.toggleShowingError('URL do instalador inválida!');
      }
    );
  }

  getComponents(endpoint: string): void {
    this.landingPageService.getComponents(endpoint).subscribe(
      resp => {
        this.installator = resp;
      },
      error => {
        console.log(error);
        this.toggleShowingError('Nenhuma instalação encontrada!');
      }
    );
  }

  getPhases(endpoint: string): void {
    this.landingPageService.getPhases(endpoint).subscribe(
      resp => {
        this.phases = resp;
      },
      error => {
        console.log(error);
        this.toggleShowingError('Nenhuma fase encontrada !');
      }
    );
  }

  toggleLoader(): void {
    this.loading = !this.loading;
  }

  toggleShowingError(message: string = ''): void {
    this.errorMessage = message;
    this.showError = !this.showError;
  }

  toggleShowingSuccess(message: string = ''): void {
    this.successMessage = message;
    this.showSuccess = !this.showSuccess;
  }

}
