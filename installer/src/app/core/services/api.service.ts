import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  options(endpoint: string): Observable<any> {
    return this.http.options(
      endpoint,
      {observe: 'response', responseType: 'text'}).pipe(
        map(
          resp => {
            if (resp.status === 200) {
              return resp.body;
            } else {
              throw { message: 'error' };
            }
          },
          error => { throw error; }
        )
    );
  }

  get(endpoint: string): Observable<any> {
    return this.http.get(endpoint, {
      observe: 'response'
    }).pipe(
      map(
        resp => {
          if (resp.status >= 200 && resp.status <= 300) {
            return resp.body;
          } else {
            throw { message: 'error' };
          }
        },
        error => {
          console.log(error);
          throw error;
        }
      )
    );
  }

  post(endpoint: string, body: any): Observable<{}> {
    // endpoint = environment.apiUrl + endpoint;
    return this.http.post(endpoint, body, {
      observe: 'response'
    })
      .pipe(
        map(
          (resp: HttpResponse<any>) => {
            if (resp.status >= 200 && resp.status <= 300) {
              return resp.body;
            } else {
              throw { message: 'error' };
            }
          },
          error => { throw error; }
        )
      );
  }

  delete(endpoint: string): Observable<{}> {
    // endpoint = environment.apiUrl + endpoint;
    return this.http.delete(endpoint, {
      observe: 'response'
    })
      .pipe(
        map(
          (resp: HttpResponse<any>) => {
            if (resp.status >= 200 && resp.status <= 300) {
              return resp.body;
            } else {
              throw { message: 'error' };
            }
          },
          error => { throw error; }
        )
      );
  }
}
