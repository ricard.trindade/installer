import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/services/api.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(
    private api: ApiService
  ) { }

  delete(endpoint: string): Observable<any> {
    return this.api.delete(endpoint).pipe(
      map(
        resp => {
          return resp;
        },
        error => {
          throw error;
        }
      )
    );
  }

  create(endpoint: string): Observable<any> {
    return this.api.post(endpoint, '').pipe(
      map(
        resp => {
          return resp;
        },
        error => {
          throw error;
        }
      )
    );
  }

  uploadFile(endpoint: string, file): Observable<any> {
    return this.api.post(endpoint, file).pipe(
      map(
        resp => {
          return resp;
        },
        error => {
          throw error;
        }
      )
    );
  }
}
