import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ApplicationComponent, Phase } from '@app/shared/models';
import { CardService } from './card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() component: ApplicationComponent;
  @Input() phase: Phase;
  @Input() endpoint: string;

  @Output() successMessage = new EventEmitter<string>();
  @Output() errorMessage = new EventEmitter<string>();

  constructor(
    private cardService: CardService
  ) { }

  ngOnInit() {
  }

  delete(name: string): void {
    console.log('delete: ' + name);
    this.cardService.delete(`${this.endpoint}/component/${name}`).subscribe(
      resp => {
        this.successMessage.emit(`Componente ${name} removido!`);
      },
      error => {
        this.errorMessage.emit(error.message);
      }
    );
  }

  create(name: string): void {
    console.log('create: ' + name);
    this.cardService.create(`${this.endpoint}/component/${name}`).subscribe(
      resp => {
        this.successMessage.emit(`Componente ${name} criado!`);
      },
      error => {
        this.errorMessage.emit(error.message);
      }
    );
  }

  add(event): void {
    console.log(event.target.files[0]);
    this.cardService.uploadFile(this.endpoint, event.target.files[0]).subscribe(
      resp => {
        this.successMessage.emit(`Fase ${this.phase.name} criado!`);
      },
      error => {
        this.errorMessage.emit(error.message);
      }
    );
  }

}
