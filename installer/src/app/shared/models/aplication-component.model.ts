import { ControlParameter, Event } from './index';

export interface ApplicationComponent {
  name: string;
  timestamp: string;
  controlParameters: ControlParameter[];
  id: string;
  metrics: any[];
  deployerParameters: any[];
  installed: boolean;
  events: Event[];
}
