export interface ControlParameter {
  name: string;
  value: string;
}
