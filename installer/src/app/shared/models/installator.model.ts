import { ControlParameter, ApplicationComponent } from './index';

export interface Installator {
  name: string;
  controlParameters: ControlParameter[];
  compliant: boolean;
  version: string;
  deployerParameters: any[];
  applicationComponents: ApplicationComponent[];
}
