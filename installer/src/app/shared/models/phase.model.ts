import { Install } from './install.model';

export interface Phase {
  name: string;
  downgrade: any[];
  version: string;
  install: Install[];
  upgrade: Install[];
  uninstall: Install[];
}
