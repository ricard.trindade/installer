export interface Event {
  timestamp: string;
  componentID: string;
  phase: string;
  componentName: string;
  commandsOutputs: any[];
  installation: string;
  status: string;
}
